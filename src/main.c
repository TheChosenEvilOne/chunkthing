#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include "window.h"
#include "loader.h"
#include "render.h"

int main() {
	if (!init_window()) return 1;
	GLFWwindow *window = get_window();
	init_render();
	int width, height;
	uint8_t *rgba;
	update_tile_texture(rgba = load_webp_as_rgba("rsc/textures/grass.webp", &width, &height), 0);
	free(rgba);
	while (!glfwWindowShouldClose(window)) {
		glClear(GL_COLOR_BUFFER_BIT);
		
		glfwPollEvents();
		glfwSwapBuffers(window);
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, GL_TRUE);
	}

	glfwTerminate();
  	return 0;
}
