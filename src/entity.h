#include <stdint.h>

struct entity_type {

};

struct entity {
	int8_t x, y;
	int32_t cX, cY;
	struct entity_type *type;
	void *data;
};

struct entity_list {
	struct entity *entity;
	struct entity_list *next;
};
