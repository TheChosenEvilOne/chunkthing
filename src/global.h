#include <stdint.h>
#include "chunk.h"
#include "defines.h"

struct chunk chunk_buffer[BUFFER_SIZE][BUFFER_SIZE];

// IMPORTANT: make sure defines in defines.h
// are correct if you modify these.
enum tile_layers {
	T_AIR,
	T_GROUND,
	T_SURFACE,
	T_BOTTOM,
};

enum entity_layers {
	E_AIR,
	E_GROUND,
};

