#pragma once

#define TILE_SIZE 8
#define CHUNK_SIZE 16
#define BUFFER_SIZE 16

#define TILE_LAYER_COUNT 4
#define ENTITY_LAYER_COUNT 2


#define OGL_VERSION_MAJOR 4
#define OGL_VERSION_MINOR 1
