#include <stdio.h>
#include <stdlib.h>
#include <webp/decode.h>

char *read_file(char *path, long *out_size);
uint8_t *load_webp_as_rgba(char *path, int *width, int *height);
