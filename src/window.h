#pragma once

#include <GLFW/glfw3.h>
#include "defines.h"
#include <stdbool.h>

bool init_window();
GLFWwindow *get_window();
