#include "render.h"

GLuint tile_texture_atlas;
GLuint tile_program;

void init_render() {
	glewExperimental = true;
	glewInit();
	glClearColor(1.f, 1.f, 1.f, 1.f);

	// Create texture atlas.
	glGenTextures(1, &tile_texture_atlas);
	glBindTexture(GL_TEXTURE_2D, tile_texture_atlas);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	// 2048 * 2048 is exactly enough for the maximum tile count.
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, 2048, 2048);

	// Initialise tile rendering.
	tile_program = create_shader_program("rsc/shaders/tileShader.glsl", "rsc/shaders/commonVert.glsl");
	glUniform1i(glGetUniformLocation(tile_program, "texture_atlas"), tile_texture_atlas);
}

void render() {
	
}

void render_chunk(struct chunk* chunk) {
	
}

// Update/Add
void update_tile_texture(uint8_t *rgba, uint16_t id) {
	glBindTexture(GL_TEXTURE_2D, tile_texture_atlas);
	glTexSubImage2D(GL_TEXTURE_2D, 0, (id & 0xFF) << 4, id >> 8 << 4, 8, 8, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, rgba);
}

GLuint compile_shader_from_file(char *path, GLenum type) {
	GLuint shader = glCreateShader(type);
	char *f = read_file(path, NULL);
	glShaderSource(shader, 1, (const char *const *) &f, NULL);
	free(f);
	glCompileShader(shader);
}

GLuint create_shader_program(char *frag_file, char *vert_file) {
	GLuint fS, vS, P;
	fS = compile_shader_from_file(frag_file, GL_FRAGMENT_SHADER);
	vS = compile_shader_from_file(vert_file, GL_VERTEX_SHADER);
	P = glCreateProgram();
	glAttachShader(P, vS);
	glAttachShader(P, fS);
	glLinkProgram(P);
	glDeleteShader(fS);
	glDeleteShader(vS);
	return P;
}
