#! /bin/bash

: "${VARIABLE_FILE:=build.vars}"
: "${FAIL_ON_ERROR:=true}"
: "${RUN:=0}"

# !! DO NOT TOUCH ANYTHING AFTER THIS LINE !!
CVRS=""
CFLGS=""
LL=""
LFLGS=""
B_TYPE="${1:-help}"
R_P=""

error () {
    >&2 echo -e "\e[31m\e[1mERROR: $1"
    $FAIL_ON_ERROR && exit 1
}

VARIABLE () {
    [ $# = 3 ] && { [ $1 = $B_TYPE ] || return; } && CVRS+="-D$2=$3 " && return
    [ $# = 2 ] && CVRS+="-D$1=$2 " && return
    error "Invalid argument count in VARIABLE definition."
}

COMPILER_FLAGS () {
    [ $# = 2 ] && { [ $1 = $B_TYPE ] || return; } && CFLGS+="$2 " && return
    [ $# = 1 ] && CFLGS+="$1 " && return
    error "Invalid argument count in COMPILER_FLAGS definition."
}

LINKER_FLAGS () {
    [ $# = 2 ] && { [ $1 = $B_TYPE ] || return; } && LFLGS+="$2 " && return
    [ $# = 1 ] && LFLGS+="$1 " && return
    error "Invalid argument count in LINKER_FLAGS definition."
}

LIBRARIES () {
    if [ $# = 2 ]; then
	[ $1 = $B_TYPE ] || return
	for L in ${2}; do
	    LL+="$(pkg-config --silence-errors --libs $L || echo -l$L) "
	done
	return
    elif [ $# = 1 ]; then
	for L in ${1}; do
	    LL+="$(pkg-config --silence-errors --libs $L || echo -l$L) "
	done
	return
    fi
    error "Invalid argument count in LIBRARIES definition."
}

RUN_WITH () {
    [ $# = 2 ] && { [ $1 = $B_TYPE ] || return; } && R_P=$2 && return
    [ $# = 1 ] && R_P=$1 && return
    error "Invalid argument count in RUN_WITH definition."
}

source $VARIABLE_FILE

help () {
    echo "TCEO's Simple Build Script."
    echo " This is a simple script for setting up building for projects quickly."
    echo " This message is displayed when the argument given to the script does not correspond to any"
    echo " of the build types listed in the variables file."
    echo " If you have moved or renamed the variables file you need to edit the variable in the build script"
    echo "Currently registered build types: ${BUILD_TYPES[@]}"
    exit
}

[[ " ${BUILD_TYPES[@]} " =~ " ${B_TYPE} " ]] || help

for FLDR in ${HEADER_FOLDERS[@]}; do
    [ ! -d $FLDR ] || error "Header folder $FLDR not found!"
    CLFGS+="-L$fldr "
done

FV=""
I=0
for FE in ${SOURCE_EXTENSIONS[@]}; do
    FV+="-name *.$FE"
    [ ${#SOURCE_EXTENSIONS[@]} = $((I+=1)) ] || FV+=" -or "
done

SFS=()
OFS=()
I=0
for F in ${SOURCE_FOLDERS[@]}; do
    [ ! -d $F ] && error "Source folder $F not found."
    SFS+=($(find $F $FV))
done

[ ${#SFS[@]} = 0 ] && error "No source files found."

for F in ${INCLUDE_SOURCE_FILES[@]}; do
    [ -f $F ] || error "Non-existant file in INCLUDE_SOURCE_FILES $F."
    SFS+=$F
done

for SCR in ${PRE_COMPILE}; do
    [ -f $SCR ] || error "Pre-compile script $SCR not found."
    $SCR
done

I=0
for F in ${SFS[@]}; do
    BF=$(dirname $BUILD_FOLDER$F)
    [ ! -d $BF ] && mkdir -p $BF
    echo -e "\e[32m[$((I+=1))/${#SFS[@]}]\e[34m Compiling: \e[0m\e[1m$F\e[0m"
    $COMPILER $CFLGS $CVRS -c $F \
        -o $BF/$(basename $F).o
    OFS+=($BF/$(basename $F).o)
done

echo -e "\e[34mLinking: \e[32m\e[1m'$EXECUTABLE_NAME'\e[0m"
$LINKER ${OFS[@]} \
	$LL \
	$LFLGS -o $EXECUTABLE_NAME
echo -e "\e[32m\e[1mFinished!\e[0m"

if [ $RUN = 1 ]; then
    $R_P $EXECUTABLE_NAME
fi

I=0
if $CLEAN_AFTER_BUILD; then
    echo -e "\e[32mCleaning up.\e[0m"
    for F in ${OFS[@]}; do
	[ -f $F ] && rm $F && echo -e "\e[32m[$((I+=1))/${#SFS[@]}]\e[34m Removed: \e[0m\e[1m$F\e[0m"
    done
fi
